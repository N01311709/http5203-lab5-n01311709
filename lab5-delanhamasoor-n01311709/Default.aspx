﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>Title: <asp:TextBox runat="server" ID="rssTitle" /></div>
            <div>Link: <asp:TextBox runat="server" ID="rssLink" /></div>
            <div>Author: <asp:TextBox runat="server" ID="rssAuth" /></div>
            <div>Description:</div>
            <asp:TextBox ID="rssDesc" TextMode="MultiLine" runat="server" />
            <div><asp:Button ID="rssSubmit" runat="server" OnClick="Add_Rss" Text="Submit" /></div>
        </div>
    </form>

    <div id="status" runat="server"></div>
</body>
</html>

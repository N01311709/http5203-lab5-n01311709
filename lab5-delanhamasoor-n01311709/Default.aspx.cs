﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Add_Rss(object sender, EventArgs e)
    {
        string xmlpath = Request.PhysicalApplicationPath + "App_Data/FlandersPress.xml";
        XmlDocument doc = new XmlDocument();

        if (System.IO.File.Exists(xmlpath))
        {
            doc.Load(xmlpath);

            // I got help for the following two lines from
            // https://stackoverflow.com/questions/919645/how-to-delete-node-from-xml-file-using-c-sharp

            XmlNode delnode = doc.SelectSingleNode("/rss/channel/item[1]");
            delnode.ParentNode.RemoveChild(delnode);

            // create and append new item

            XmlNode root = doc.SelectSingleNode("/rss/channel");
            XmlNode article = createRssItem(doc);
            root.AppendChild(article);
            status.InnerHtml = "Added";
        }
        else
        {
            status.InnerHtml = "Sorry, file not found or we have pivoted to video.";
        }
        doc.Save(xmlpath);
    }

    XmlNode createRssItem(XmlDocument doc)
    {
        // function for creating used inside the main function

        // step one, create elements

        XmlNode article = doc.CreateElement("item");

        XmlNode title = doc.CreateElement("title");
        XmlNode link = doc.CreateElement("link");
        XmlNode author = doc.CreateElement("author");
        XmlNode pubdate = doc.CreateElement("pubDate");
        XmlNode desc = doc.CreateElement("description");

        // step two, fill them with content from the form or otherwise with a datetime stamp

        title.InnerText = rssTitle.Text;
        link.InnerText = rssLink.Text;
        author.InnerText = rssAuth.Text;
        pubdate.InnerText = DateTime.Today.ToString();
        desc.InnerText = rssDesc.Text;

        // step three, append

        article.AppendChild(title);
        article.AppendChild(link);
        article.AppendChild(author);
        article.AppendChild(pubdate);
        article.AppendChild(desc);

        return article;
    }
}